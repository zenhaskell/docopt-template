---
title: Getting Started
---

docopt-template has two commands, `cat` and `echo`. Here are some example use cases

```bash
app-exe cat foo.txt
```

```bash
app-exe echo --caps foo # prints "FOO"
```
