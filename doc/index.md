---
title: Home
---

# docopt-template - A simple application using the docopt library.

This is the soft documentation site for the docopt-template.

This serves as a place to provide detailed descriptions about how to
use your application or library.

![build](https://gitlab.com/zenhaskell/docopt-template/badges/master/build.svg)
